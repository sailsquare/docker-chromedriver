# Docker Chromedriver/Chrome Headless Image

This is a Docker image to run e2e tests via Chrome Headless.

It can be used as a `chromedriver` stand-alone executable:
```bash
$: docker run -ti --rm registry.gitlab.com/sailsquare/docker-chromedriver --help

Usage: chromedriver [OPTIONS]

Options
  --port=PORT                     port to listen on
  --adb-port=PORT                 adb server port
  --log-path=FILE                 write server log to file instead of stderr, increases log level to INFO
  --verbose                       log verbosely
  --version                       print the version number and exit
  --silent                        log nothing
  --url-base                      base URL path prefix for commands, e.g. wd/url
  --port-server                   address of server to contact for reserving a port
  --whitelisted-ips               comma-separated whitelist of remote IPv4 addresses which are allowed to connect to ChromeDriver
```

Please note that the image is configured to only expose the default port `9515`, so changing it via CLI flag will not work.

The `--whitelisted-ips` flag will likely need to be specified, because Chromdriver only listens for local connection by default. Set it to an empty string (`--whitelisted-ips=''`) to allow any ip.

You can add custom certificates by loading them in the `/docker-entrypoint-certs.d/` directory, in a fashion similar to [MySQL Docker image fixtures](https://github.com/docker-library/docs/tree/master/mysql#initializing-a-fresh-instance).  
These will then be loaded at runtime by the container entrypoint script.
