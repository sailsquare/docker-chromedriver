# largely copied from https://hub.docker.com/r/robcherry/docker-chromedriver

FROM debian:jessie

# Install Utils, Chrome WebDriver and Google Chrome
RUN apt-get -y update && \
    apt-get -y install curl unzip libnss3-tools && \
    CHROMEDRIVER_VERSION=`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE` && \
    mkdir -p /opt/chromedriver-$CHROMEDRIVER_VERSION && \
    curl -sS -o /tmp/chromedriver_linux64.zip http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip && \
    unzip - /tmp/chromedriver_linux64.zip -d /opt/chromedriver-$CHROMEDRIVER_VERSION && \
    rm /tmp/chromedriver_linux64.zip && \
    chmod +x /opt/chromedriver-$CHROMEDRIVER_VERSION/chromedriver && \
    ln -fs /opt/chromedriver-$CHROMEDRIVER_VERSION/chromedriver /usr/local/bin/chromedriver && \
    curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list && \
    apt-get -y update && \
    apt-get -y install google-chrome-stable && \
    rm -rf /var/lib/apt/lists/*

# add a small init system
ENV TINI_VERSION v0.16.1
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /sbin/tini
RUN chmod +x /sbin/tini

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN mkdir /docker-entrypoint-certs.d
ENTRYPOINT ["/sbin/tini", "--", "docker-entrypoint.sh"]

EXPOSE 9515
RUN useradd chrome --shell /bin/bash --create-home
USER chrome

# prepare certutils to add custom certificates
RUN mkdir -p $HOME/.pki/nssdb \
    && certutil -d sql:$HOME/.pki/nssdb -N --empty-password

CMD [ "chromedriver" ]
