#!/usr/bin/env bash

set -e

# install certificates mounted/copied in the following dir
CERT_DIR=/docker-entrypoint-certs.d
for CERT_FILE in $(find ${CERT_DIR} -type f); do
    if ! certutil -d sql:$HOME/.pki/nssdb -L -n "${CERT_FILE}" &> /dev/null ; then
        certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n "${CERT_FILE}" -i "${CERT_FILE}"
        echo "${CERT_FILE} added via certutil"
    fi
done

# copied form
# https://github.com/docker-library/php/blob/662067f7336bbf238fdffb3aeee4b084a0cf3de7/7.1/jessie/cli/docker-php-entrypoint
# allows usage of the image as a fire-and-forget command by passing flags directly to the chromedriver binary
if [ "${1#--}" != "$1" ]; then # if first arg starts with `--`
	set -- chromedriver "$@" # prepend chromedriver binary path to the arguments
fi

exec $@

